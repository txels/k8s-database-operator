# Dockerfile for creating a statically-linked Rust application using docker's
# multi-stage build feature. This also leverages the docker build cache to avoid
# re-downloading dependencies if they have not changed.
FROM clux/muslrust:1.54.0 AS build

# Create a dummy project just to build the app's dependencies in an early layer.
WORKDIR /src
RUN USER=root cargo new project
WORKDIR /src/project
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# Copy the source and build the application.
COPY src ./src

RUN cargo install --target x86_64-unknown-linux-musl --root /target --path .

# FINAL layer with single statically linked binary
# Copy the statically-linked binary into a scratch container.
#FROM alpine
FROM scratch

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /target/bin/k8s-database-controller .

ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_DIR=/etc/ssl/certs
ENV RUST_LOG=info

USER 1000
CMD ["./k8s-database-controller"]
