use kube::api::{Api, ListParams};
use kube::Client;

pub mod controller;
pub mod crd;
pub mod finalizer;
pub mod secrets;

pub use crd::Database;

pub async fn list_databases(client: Client, namespace: &str) -> Result<(), kube::Error> {
    let db_api: Api<Database> = Api::namespaced(client, namespace);
    let lp = ListParams::default();
    let all_dbs = db_api.list(&lp).await?;

    for db in all_dbs {
        let name = match &db.metadata.name {
            Some(n) => n,
            None => "Not found",
        };

        let username = match &db.spec.username {
            Some(n) => n,
            None => "Not found",
        };

        println!("Got database: name={:?} username={:?}", name, username);
    }
    Ok(())
}
