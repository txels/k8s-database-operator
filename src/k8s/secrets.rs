use k8s_openapi::api::core::v1::Secret;
use kube::api::{DeleteParams, ListParams, PostParams};
use kube::client::Client;
use kube::Api;

pub async fn list(client: Client, namespace: &str) -> Result<(), kube::Error> {
    let secret_api: Api<Secret> = Api::namespaced(client, namespace);
    let lp = ListParams::default();
    let all_secrets = secret_api.list(&lp).await?;

    for secret in all_secrets {
        let name = match &secret.metadata.name {
            Some(n) => n,
            None => "Not found",
        };

        println!("Got secret: {:?}", name);
    }
    Ok(())
}

pub async fn create(
    client: Client,
    name: &str,
    namespace: &str,
    username: &str,
    password: &str,
) -> Result<(), kube::Error> {
    let secrets: Api<Secret> = Api::namespaced(client, namespace);

    // secrets.delete("db-secret-rust").await?;

    // Create a secret from JSON
    let secret = serde_json::from_value(serde_json::json!({
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": name
        },
        "type": "Opaque",
        "data": {
            "username": base64::encode(username),
            "password": base64::encode(password),
        }
    }))?;

    // Create the secret
    secrets.create(&PostParams::default(), &secret).await?;

    Ok(())
}

pub async fn delete(client: Client, name: &str, namespace: &str) -> Result<(), kube::Error> {
    let api: Api<Secret> = Api::namespaced(client, namespace);
    api.delete(name, &DeleteParams::default()).await?;
    Ok(())
}
