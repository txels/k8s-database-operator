use futures::stream::StreamExt;
use kube::api::ListParams;
use kube::client::Client;
use kube::{Api, Resource, ResourceExt};
use kube_runtime::controller::{Context, ReconcilerAction};
use kube_runtime::Controller;
use log::*;
use tokio::time::Duration;

use super::crd::Database;
use super::finalizer;
use crate::database::DBServer;
use crate::k8s::secrets;
use crate::random::rand_password;

pub async fn run(dbserver: DBServer) -> () {
    let client: Client = Client::try_default()
        .await
        .expect("Expected a valid KUBECONFIG environment variable.");

    let crd_api: Api<Database> = Api::all(client.clone());
    let context: Context<ContextData> = Context::new(ContextData::new(client.clone()));

    Controller::new(crd_api.clone(), ListParams::default())
        .run(
            |db, context| reconcile(dbserver.clone(), db, context),
            on_error,
            context,
        )
        .for_each(|reconciliation_result| async move {
            match reconciliation_result {
                Ok(resource) => {
                    debug!("Reconciliation successful. Resource: {:?}", resource);
                }
                Err(reconciliation_err) => {
                    error!("Reconciliation error: {:?}", reconciliation_err)
                }
            }
        })
        .await;
}

/// Context injected with each `reconcile` and `on_error` method invocation.
struct ContextData {
    /// Kubernetes client to make Kubernetes API requests with. Required for K8S resource management.
    client: Client,
}

impl ContextData {
    /// Constructs a new instance of ContextData.
    ///
    /// # Arguments:
    /// - `client`: A Kubernetes client to make Kubernetes REST API requests with. Resources
    /// will be created and deleted with this client.
    pub fn new(client: Client) -> Self {
        ContextData { client }
    }
}

/// Action to be taken upon a resource during reconciliation
#[derive(Debug)]
enum Action {
    Create,
    Delete,
    Consolidate,
}

async fn reconcile(
    dbserver: DBServer,
    db: Database,
    context: Context<ContextData>,
) -> Result<ReconcilerAction, Error> {
    let client: Client = context.get_ref().client.clone();

    let name = db.name();
    let secret_name = format!("managed-{}-database", name);
    let username = match &db.spec.username {
        Some(n) => n,
        None => &name,
    };

    let namespace: String = match db.namespace() {
        None => {
            return Err(Error::UserInputError(
                "Expected Echo resource to be namespaced. Can't deploy to an unknown namespace."
                    .to_owned(),
            ));
        }
        Some(namespace) => namespace,
    };

    let result = determine_action(&db);
    debug!("Reconcile action: {:?}", result);

    return match result {
        Action::Create => {
            let password = rand_password();
            info!(
                "Creating secret={} in namespace={}, username={}",
                name, namespace, username,
            );
            // TODO: handle errors with secret creation
            secrets::create(
                client.clone(),
                &secret_name,
                &namespace,
                &username,
                &password,
            )
            .await?;
            // add finalizer once resources exist...
            // TODO: create separate finalizers for secret and database
            // for a more effective cleanup
            finalizer::add(client.clone(), &name, &namespace).await?;
            info!("Creating database={}, username={}", name, username);
            dbserver.create_database(&username, &password).await?;
            Ok(ReconcilerAction {
                requeue_after: Some(Duration::from_secs(10)),
            })
        }
        Action::Delete => {
            secrets::delete(client.clone(), &secret_name, &namespace).await?;
            let result = dbserver.drop_database(&username).await;
            match result {
                Ok(_) => info!("Dropped database {}", username),
                Err(v) => warn!("Could not drop database {}", v),
            }
            finalizer::delete(client.clone(), &name, &namespace).await?;
            Ok(ReconcilerAction {
                // Makes no sense to delete after a successful delete, as the resource is gone
                requeue_after: None,
            })
        }
        Action::Consolidate => {
            // In theory we've created the resources, but there may have been failures
            // e.g. database connection may have failed
            // TODO: check if secret exists, retrieve credentials, check if DB exists,
            // if not create using retrieved credentials
            Ok(ReconcilerAction {
                requeue_after: Some(Duration::from_secs(10)),
            })
        }
    };
}

/// Resources arrives into reconciliation queue in a certain state. This function looks at
/// the state of given resource and decides which actions needs to be performed.
/// The finite set of possible actions is represented by the `Action` enum.
fn determine_action(db: &Database) -> Action {
    return if db.meta().deletion_timestamp.is_some() {
        Action::Delete
    } else if db.meta().finalizers.is_empty() {
        Action::Create
    } else {
        Action::Consolidate
    };
}

fn on_error(error: &Error, _context: Context<ContextData>) -> ReconcilerAction {
    error!("Reconciliation error:\n{:?}", error);
    ReconcilerAction {
        requeue_after: Some(Duration::from_secs(5)),
    }
}

/// All errors possible to occur during reconciliation
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Any error originating from the `kube-rs` crate
    #[error("Kubernetes reported error: {source}")]
    KubeError {
        #[from]
        source: kube::Error,
    },
    /// Any error from the tokio-postgres crate
    #[error("Tokio-postgres reported error: {source}")]
    PostgresError {
        #[from]
        source: tokio_postgres::Error,
    },
    /// Error in user input or db resource definition, typically missing fields.
    #[error("Invalid CRD: {0}")]
    UserInputError(String),
}
