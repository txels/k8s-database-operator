extern crate rand;

pub fn rand_password() -> String {
    let rand: [u8; 24] = rand::random();
    let rhex: String = rand.iter().map(|x| format!("{:x}", x)).collect();
    rhex
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn rand_passwords_are_long() {
        let pass = rand_password();
        assert!(pass.len() >= 40);
    }

    #[test]
    fn rand_passwords_do_not_repeat() {
        let pass1 = rand_password();
        let pass2 = rand_password();
        assert_ne!(pass1, pass2);
    }
}
