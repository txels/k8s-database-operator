use log::*;
use std::env;
use std::error::Error;

pub mod database;
pub mod k8s;
pub mod random;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    let host = env::var("DB_HOST").unwrap_or_else(|_| "localhost".to_owned());
    let user = env::var("DB_USER").unwrap_or_else(|_| "postgres".to_owned());
    let password = env::var("DB_PASS").unwrap_or_else(|_| "postgres".to_owned());

    info!("Started controller: DB host={} user={}", host, user);
    let connect_str = format!("host={} user={} password={}", host, user, password);
    let dbserver = database::DBServer::new(connect_str);
    k8s::controller::run(dbserver).await;
    Ok(())
}
