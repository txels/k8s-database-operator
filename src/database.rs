use log::*;
use std::sync::Arc;
use tokio::task::JoinHandle;
use tokio_postgres::{Client, Error, NoTls};

#[derive(Debug, Clone)]
pub struct DBServer {
    connect_str: String,
}

impl DBServer {
    pub fn new(connect_str: String) -> Self {
        Self { connect_str }
    }

    async fn connect(&self) -> Result<Arc<(Client, JoinHandle<()>)>, Error> {
        let (client, connection) = tokio_postgres::connect(&self.connect_str, NoTls).await?;

        let handle = tokio::spawn(async move {
            if let Err(e) = connection.await {
                error!("connection error: {}", e);
            }
        });

        Ok(Arc::new((client, handle)))
    }

    async fn execute(&self, queries: &Vec<String>) -> Result<(), Error> {
        let result = self.connect().await?;
        let client = &result.0;
        let handle = &result.1;

        for query in queries {
            let result = client.execute(&query[..], &[]).await?;
            // TODO: error handling - properly propagate DB creation error
            info!("Query '{}'\nReturned {:?}", query, result);
        }

        // close the tokio task... we don't want to keep tasks around
        handle.abort();
        Ok(())
    }

    pub async fn create_database(&self, user: &str, password: &str) -> Result<(), Error> {
        let queries = vec![
            format!(
                "create user {user} password '{password}'",
                user = user,
                password = password
            ),
            format!("create database {user} owner {user}", user = user),
        ];
        self.execute(&queries).await
    }

    pub async fn drop_database(&self, user: &str) -> Result<(), Error> {
        let queries = vec![
            format!("drop database {user}", user = user),
            format!("drop user {user}", user = user),
        ];
        self.execute(&queries).await
    }

    pub async fn disable_user(&self, user: &str) -> Result<(), Error> {
        let queries = vec![format!("alter user {user} with nologin", user = user)];
        self.execute(&queries).await
    }
}
