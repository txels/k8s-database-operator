# A k8 controller to manage accounts

A prototype for a more ambitious project, this is both my first non-trivial
program written in Rust and my first kubernetes operator.

The initial version of this project had 3 main goals:

- Learn Rust using a project-based approach
- Learn how to build a kubernetes operator
- Build automation for some of the repetitive tasks I do for my personal projects

## The database account controller

A typical thing I always do before deploying my prototype Django projects
(backed by a Postgres database) for the first time, is create a new database
and its owner user for each service, initialised with a random password.
I then store that as a secret that the service can retrieve at startup.

I have these steps partially automated in shell scripts, but I felt that
deploying a k8s _database resource_ as part of the service could make things much simpler:
I could include in my base "new service" template not just the k8s Deployment,
Service and Ingress resources that it typically consists of, but also a custom
"database" resource.

## The Database custom resource

The starting point for this operator is a kubernetes custom resource, which
is the mechanism by which we can extend the capabilities of the k8s API.

In order to define a custom resource for our database, we have to create a
`CustomResourceDefinition` (CRD).

Our CRD, included as `db-crd.yml` in this repo, defines a new `Database` resource.
It can be instantiated in our clusters like any other k8s resource:

    kubectl apply -f db-crd.yml

Once we've done that, the k8s API will know about this new type of resource
and we will be able to create new `Database` instances with definitions
similar to this one:

```yaml
# db.yml
apiVersion: k8s.txels.com/v1
kind: Database
metadata:
  name: fretshare
spec:
  username: fretshare
```

This definition is saying that we want to create a database for the username
`fretshare` (which will be the name of the service using the database).

## The controller

The controller is the rust application that will respond to events for this
`Database` resource kind (it will _own_ the custom resource): it will be told
about changes to resources of this type which will include creation, updates and
deletions of `Database` instances.

Kubernetes is a declarative platform, where users tell it which resources should
exist in the cluster, and the operators take the required actions to ensure that
desired state matches actual state all the time. This process is called
reconciliation.

The job of our operator will be to listen to changes to the collection of
`Database` resources and make sure the underlying resources (secrets and
postgres databases) match.

A well-rounded operator should take care as well of accidental changes to
the underlying resources (e.g. someone deletes the secret by mistake?)
and recreate them. (That is what replicaset and deployment controllers do
e.g. if pods die or get deleted.)

## Development

Build with `cargo build` as you would any Rust cargo project.

Package into a docker image with `make package`

The controller relies on 3 environment variables to connect to the database:
`DB_HOST`, `DB_USER` and `DB_PASS`.

Run unit tests with `cargo test`.

Run the smoke test (integration):

- You should have a kubernetes cluster accessible and permissions to create CRDs
- Run `make run` in one shell (if you haven't installed
  the CRD yet it will log some 404 errors, but that's fine: th k8s API we need is not
  yet available)
- Run `make smoke-test` on a separate shell

## Additional resources

- https://kubernetes.io/docs/concepts/extend-kubernetes/operator/
