IMAGE?=k8s-database-controller
VERSION?=latest


release:
	cargo build --release
	strip target/release/${IMAGE}

deps:
	cargo tree -f "{p} {f}"

doc:
	cargo doc --open

package:
	docker build -t ${IMAGE}:${VERSION} .

run:
	docker-compose up --build

install-crd:
	kubectl apply -f db-crd.yml

test:
	@cargo test
	@make smoke-test

smoke-test: install-crd
	@. ./smoke-test && run


.PHONY: release deps doc package drun smoke-test
